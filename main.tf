provider "google" {
  credentials = file("optimum-agency-316611-d85ca8117e80.json")
  project = "${var.project}"
  region  = "${var.region}"
  zone    = "${var.zone}"
}

data "google_compute_address" "wtf" {
	count = "${var.instance_count}"
	name = "ip${count.index+1}"
}


resource "google_compute_instance" "vm_instance" {
  count = var.instance_count
  name         = "${element(var.instance_tags, count.index)}"
  machine_type = "${var.type}"

  boot_disk {
    initialize_params {
      image = "centos-7-v20200403"
    }
  }
  metadata = {
		ssh-keys = "root:${file(var.public_key_path)}"
	}

    metadata_startup_script = "sed -i \"s/PermitRootLogin no/PermitRootLogin yes/g\" /etc/ssh/sshd_config && sed -i \"s/PasswordAuthentication no/PasswordAuthentication yes/g\" /etc/ssh/sshd_config && service sshd restart && sudo chmod 777 /home/ && echo 'echo -e \"root\\nroot\" | passwd root' >> /home/script.sh && sudo chmod a+x /home/script.sh && sudo -H -u root bash -c \"sh /home/script.sh\""
  network_interface {
    # A default network is created for all GCP projects
    network = google_compute_network.vpc_network.self_link
    access_config {
      nat_ip = "${data.google_compute_address.wtf["${count.index}"].address}"
    }
  }
  tags = ["http-server"]
}

resource "google_compute_network" "vpc_network" {
  name                    = "terraform-network"
  auto_create_subnetworks = "true"
}

resource "google_compute_firewall" "http-server" {
  name    = "default-allow-http1"
  network = google_compute_network.vpc_network.name

  allow {
    protocol = "tcp"
    ports    = ["80","22","8081", "8080"]
  }

  // Allow traffic from everywhere to instances with an http-server tag
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["http-server"]
}
