variable "region" {
    default = "europe-west3"
}

variable "zone" {
    default = "europe-west3-c"
}

variable "project" {
    default = "optimum-agency-316611"
}

variable "type" {
    default = "e2-medium"
}

variable "public_key_path" {
    description = "Path to file containing public key"
    default     = "key.pub"
}

variable "instance_count" {
  default = 2
}
variable "instance_tags" {
  type = "list"
  default = [ "nexus-jenkins", "awx"]
}
